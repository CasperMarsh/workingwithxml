﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSession 
{
    private string shipName;
    private string imageName;
    private int maxspeed;
    private int crew;
    private int length;

    public string Ship { get => shipName; }
    public string ImageName { get => imageName; }
    public int Maxspeed { get => maxspeed; }
    public int Crew { get => crew; }
    public int Length { get => length; }


    public ShipSession(string ship, string imageName, int maxspeed, int crew, int length)
    {
        Debug.Log(maxspeed);
        this.shipName = ship;
        this.imageName = imageName;
        this.maxspeed = maxspeed;
        this.crew = crew;
        this.length = length;
    }
}
