﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{

	private ShipSession[] shipsessions;

	[SerializeField]
	private int currentCardCount = 0;


    void Start()
    {
		shipsessions = MyUtilities.LoadXMLData("shipsessions");
		DisplayCard(currentCardCount); ;		
	}

	public void pickNextCard()
	{
		currentCardCount++;
		if (currentCardCount == shipsessions.Length) currentCardCount = 0;

		DisplayCard(currentCardCount);
	}

	private void DisplayCard(int currentCardCount)
	{
        Debug.Log(currentCardCount);
		GameObject.Find("Ship").GetComponent<Text>().text = shipsessions[currentCardCount].Ship;
		GameObject.Find("Image").GetComponent<Image>().sprite = Resources.Load<Sprite>(shipsessions[currentCardCount].ImageName);

		GameObject.Find("MaxSpeed").GetComponent<Text>().text = "Max Safe Speed: "+shipsessions[currentCardCount].Maxspeed;
		GameObject.Find("Crew").GetComponent<Text>().text = "Crew Compliment: " +shipsessions[currentCardCount].Crew;
		GameObject.Find("Length").GetComponent<Text>().text = "Length of Ship: " +shipsessions[currentCardCount].Length;
	}
}
