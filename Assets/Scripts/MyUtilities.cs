using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{    

    public static ShipSession[] LoadXMLData(string filename)
    {
		List<ShipSession> cards = new List<ShipSession>();

		string shipName = "";
		string imageName = "";
		int numberOfSpeed = 0;
		int numberOfCrew = 0;
		int numberOfLength = 0;
		

		TextAsset xmlData = Resources.Load(filename) as TextAsset;
        string xmlString = xmlData.text;

        ShipSession session = null;

        // Create an XmlReader
        using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
        {
            while (reader.Read())
            {

                if (reader.LocalName == "class" && reader.IsStartElement())
                {
                    session = null;
                }
                if (reader.IsStartElement())
                {
                    switch (reader.LocalName)
                    {
                        case "ship":
                            shipName = reader.ReadElementContentAsString();
                            break;
                        case "image":
                            imageName = reader.ReadElementContentAsString();
                            break;
                        case "maxspeed":
                            numberOfSpeed = reader.ReadElementContentAsInt();
                            break;
                        case "crew":
                            numberOfCrew = reader.ReadElementContentAsInt();
                            break;
                        case "length":
                            numberOfLength = reader.ReadElementContentAsInt();
                            break;
                    }
                }
				if (reader.LocalName == "class" && !reader.IsStartElement())
				{
					session = new ShipSession(shipName,
									imageName,
									numberOfSpeed,
									numberOfCrew,
									numberOfLength);
					cards.Add(session);
				}
			}
        }   

		return cards.ToArray();
	}
}
